import Vue from 'vue'
import Vuex, { mapState } from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
  key: 'vuex', // The key to store the state on in the storage provider.
  storage: window.localStorage // or window.sessionStorage or localForage
  // Function that passes the state and returns the state with only the objects you want to store.
  // reducer: state => state,
  // Function that passes a mutation and lets you decide if it should update the state in localStorage.
  // filter: mutation => (true)
})

export default new Vuex.Store({
  computed: {
    ...mapState(['searchItems', 'shoppingItems'])
  },

  state: {
    darkTheme: false,
    iconAttribution: 'Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> and <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>',
    ip: '',
    mapbox_access_token: process.env.VUE_APP_MAPBOX_ACCESS_TOKEN,
    mapLng: 0,
    mapLat: 0,
    mapZoom: 0,
    userLng: 0,
    userLat: 0,
    shoppingItems: [],
    shoppingTripList: {},
    shoppingTripListChecked: {}
  },

  mutations: {
    addItemToShoppingList (state, payload) {
      const index = state.shoppingItems.map(elem => elem.id).indexOf(payload.id)
      if (index !== -1) {
        Vue.set(state.shoppingItems[index], 'quantity', state.shoppingItems[index].quantity + 1)
      }
      else {
        payload.quantity = 1
        state.shoppingItems.push(payload)
      }
    },

    checkShoppingListItem (state, payload) {
      // console.log(state.shoppingTripListChecked[payload])
      state.shoppingTripListChecked[payload] = !state.shoppingTripListChecked[payload]
    },

    clearShoppingItems (state) {
      state.shoppingItems = []
    },

    clearShoppingTripList (state) {
      state.shoppingTripList = {}
    },

    clearShoppingTripListChecked (state) {
      state.shoppingTripListChecked = {}
    },

    decreaseShoppingItemQuantity (state, payload) {
      // Vue.set(state.shoppingItems, payload, state.shoppingItems[payload] - 1)
      var item = state.shoppingItems.filter(item => item.id === payload)[0]
      item.quantity -= 1
      // Vue.set(item, item.quantity - 1)
    },

    increaseShoppingItemQuantity (state, payload) {
      // Vue.set(state.shoppingItems, payload, state.shoppingItems[payload] + 1)
      var item = state.shoppingItems.filter(item => item.id === payload)[0]
      item.quantity += 1
      // Vue.set(item, item.quantity + 1)
    },

    removeShoppingItem (state, payload) {
      state.shoppingItems = state.shoppingItems.filter(item => item.id !== payload)
    },

    toggleDarkTheme (state) {
      state.darkTheme = !state.darkTheme
    },

    setIP (state, payload) {
      state.ip = payload
    },

    setMapLoc (state, payload) {
      state.mapLng = payload.lngLat[0]
      state.mapLat = payload.lngLat[1]
      state.mapZoom = payload.zoom
    },

    setShoppingTripList (state, payload) {
      state.shoppingTripList = payload
    },

    setUserLoc (state, payload) {
      state.userLng = payload.lng
      state.userLat = payload.lat
    },

    updateMapStyle (state) {
      if (state.darkTheme) {
        state.mapStyle = 'mapbox://styles/mapbox/dark-v10'
      }
      else {
        state.mapStyle = 'mapbox://styles/mapbox/light-v10'
      }
    }
  },

  actions: {

  },

  plugins: [vuexLocalStorage.plugin]
})

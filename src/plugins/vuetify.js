import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'mdi'
  },
  theme: {
    dark: true,
    themes: {
      dark: {
        accent: '#EB306F',
        background: '#000000',
        header: '#000000'
      },
      light: {
        accent: '#EB306F'
      }
    }
  }
})

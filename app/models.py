from app import db

vendor_item_table = db.Table(
    'vendor_item_table',
    db.Column(
        'vendor_id',
        db.BigInteger,
        db.ForeignKey('vendor.id')
    ),
    db.Column(
        'item_id',
        db.Integer,
        db.ForeignKey('item.id')
    ),
    db.PrimaryKeyConstraint('vendor_id', 'item_id')
)


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True)
    description = db.Column(db.String, index=True)
    icon = db.Column(db.String)
    vendors = db.relationship(
        'Vendor',
        secondary=vendor_item_table,
        backref=db.backref('items')
    )

    def __repr__(self):
        return '<Item {}>'.format(self.name)

    def as_dict(self):
        selfDict = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        selfDict['vendors'] = [v.id for v in self.vendors]
        return selfDict


class MissingItemReport(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    itemID = db.Column(db.Integer)
    vendorID = db.Column(db.BigInteger)
    datetime = db.Column(db.DateTime)

    def __repr__(self):
        return '<MissingItemReport {} {} {}>'.format(self.itemID, self.vendorID, self.datetime)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Vendor(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    addr_city = db.Column(db.String)
    addr_housenumber = db.Column(db.String)
    addr_postcode = db.Column(db.String)
    addr_street = db.Column(db.String)
    brand = db.Column(db.String)
    description = db.Column(db.String)
    lat = db.Column(db.Float, index=True)
    lon = db.Column(db.Float, index=True)
    name = db.Column(db.String)
    opening_times = db.Column(db.String)
    type = db.Column(db.String)
    wheelchair = db.Column(db.String)
    #items = db.relationship(
    #    'Item',
    #    secondary=vendor_item_table,
    #    backref=db.backref('vendors')
    #)

    def __repr__(self):
        return '<Vendor {}>'.format(self.id)

    def as_dict(self):
        selfDict = {c.name: getattr(self, c.name) for c in self.__table__.columns}
        # selfDict['items'] = [i.id for i in self.items]
        selfDict['items'] = [item.as_dict() for item in self.items]
        return selfDict

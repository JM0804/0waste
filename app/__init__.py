from flask import Flask
from flask_compress import Compress
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
Compress(app)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app import models, routes

#from app import _itemstodb
#_itemstodb.insertItems()

if __name__ == '__main__':
    app.run(
        debug=True,
        use_reloader=True
    )

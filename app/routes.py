from flask import jsonify, request, send_from_directory
from app import app, db, models
from pathlib import Path
from scipy import spatial
import collections
import datetime


@app.route('/')
def index():
    return send_from_directory('./static', 'index.html')


@app.route('/api')
def api():
    return 'API', 200


@app.route('/api/missing_items', methods=['POST'])
def missing_items():
    pairs = request.get_json()
    now = datetime.datetime.now()
    print(pairs)
    for pair in pairs:
        missingItemReport = models.MissingItemReport()
        missingItemReport.itemID = pair[0]
        missingItemReport.vendorID = pair[1]
        missingItemReport.datetime = now
        db.session.add(missingItemReport)
    db.session.commit()
    for m in models.MissingItemReport.query.all():
        print(m)
    return '', 200


@app.route('/api/search_item/<term>')
def item(term=""):
    items = models.Item.query.filter(
        models.Item.name.contains(term.lower())
    ).all()
    result = [item.as_dict() for item in items]
    return jsonify(result)


@app.route('/api/trip', methods=['POST'])
def trip():
    content = request.get_json()
    lngLat = [(content['location'][0], content['location'][1])]
    shoppingLocations = collections.defaultdict(
        lambda: collections.defaultdict(list)
    )

    # Generate list of Item models from requested item IDs,
    # sorted by item name
    items = sorted(
        [models.Item.query.get(itemID) for itemID in content['items']],
        key=lambda i: i.name
    )

    for item in items:
        if item.vendors:
            # Generate a list of vendor co-ordinates and a list of vendor IDs
            vendorCoords = [(v.lon, v.lat) for v in item.vendors]
            vendorIDs = [v.id for v in item.vendors]
            # Get the closest shop that sells the item and its ID
            # and add it to the list with the corresponding item
            # Reference: https://stackoverflow.com/a/39109296/4805625
            distance, id = spatial.KDTree(vendorCoords).query(lngLat)
            vendorID = vendorIDs[id[0]]
            # Store the ID of the vendor if not already done
            if 'id' not in shoppingLocations[vendorID]:
                shoppingLocations[vendorID]['id'] = vendorID
            # Store the name of the vendor if not already done
            if 'name' not in shoppingLocations[vendorID]:
                shoppingLocations[vendorID]['name'] = \
                    models.Vendor.query.get(vendorID).name
            # Store the distance to the vendor if not already done
            # (cut to three decimal places)
            if 'distance' not in shoppingLocations[vendorID]:
                shoppingLocations[vendorID]['distance'] = float(distance[0])
            # Add the item ID to the list
            shoppingLocations[vendorID]['items'].append(item.id)
        else:
            # If there are no vendors that sell the item,
            # add the item to the not available list
            # Store the ID of the vendor if not already done
            if 'id' not in shoppingLocations[0]:
                shoppingLocations[0]['id'] = 0
            if 'name' not in shoppingLocations[0]:
                shoppingLocations[0]['name'] = 'Not Available'
            if 'distance' not in shoppingLocations[0]:
                shoppingLocations[0]['distance'] = 999999999999
            shoppingLocations[0]['items'].append(item.id)
    return jsonify(shoppingLocations)


# @app.route('/api/vendors/<userLat>/<userLon>')
# def vendors(userLat="", userLon=""):
#     vendors_query = models.Vendor.query.all()
#     vendors = []
#     for vendor in vendors_query:
#         vendors.append(vendor.as_dict())
#     return jsonify(vendors)


@app.route('/api/vendor/')
@app.route('/api/vendor/<id>')
def vendor(id=""):
    vendor = models.Vendor.query.get(id)
    return jsonify(vendor.as_dict())


@app.route('/api/vendor_geojson/<id>')
def vendor_geojson(id=""):
    vendor = models.Vendor.query.get(id)
    geojson = {
        'type': 'Feature',
        'properties': {
            'type': vendor.type
        },
        'geometry': {
            'type': 'Point',
            'coordinates': [vendor.lon, vendor.lat]
        }
    }
    return geojson


@app.route('/<path:path>')
def catch_all(path):
    if Path(path).suffix == '.geojson':
        # Override .geojson mimetype
        return send_from_directory(
            './static',
            path,
            mimetype='application/geo+json'
        )
    return send_from_directory('./static', path)


@app.errorhandler(404)
def error_404(e):
    return 'This route does not exist {}'.format(request.url), 404

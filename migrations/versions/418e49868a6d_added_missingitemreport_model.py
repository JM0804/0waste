"""Added MissingItemReport model

Revision ID: 418e49868a6d
Revises: 14be2ba8b94a
Create Date: 2020-05-11 03:52:02.388426

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '418e49868a6d'
down_revision = '14be2ba8b94a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('missing_item_report',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('itemID', sa.Integer(), nullable=True),
    sa.Column('vendorID', sa.BigInteger(), nullable=True),
    sa.Column('datetime', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('missing_item_report')
    # ### end Alembic commands ###

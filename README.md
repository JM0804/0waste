# Zero Waste Web App

https://0waste.app/

## Requirements

- Python 3.9.x
- Pipenv (latest)
- PNPM 6.x
- NodeJS 12.x
- [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) (if you plan on pushing to Heroku)

## Setting up the app

    $ pipenv --three
    $ pipenv shell
    $ pipenv install
    $ pnpm install
    $ pnpm build

You will also need to set your access tokens. Create a file called `.env` and populate it as follows:

>VUE_APP_MAPBOX_ACCESS_TOKEN='token goes here'

## Running the app

    $ pipenv shell
    $ ./run.sh

## Deploying to Heroku

    $ git push heroku master

You will also need to install the repo plugin:

    $ heroku plugins:install heroku-repo

### To force a rebuild (e.g. after changing environment variables)

    $ heroku repo:purge_cache
    $ heroku repo:reset
    $ git push heroku master

### To push the local PostgreSQL database

    $ heroku pg:reset DATABASE_URL
    $ heroku pg:push zerowaste DATABASE_URL --app jm0804-0wasteapp

## Developing the app

### Lint and fix files

    $ pnpm lint

### Upgrade all Python packages (including their pins)

    $ pipenv update --outdated

### Upgrade all JavaScript packages (including their pins)

    $ pnpm update

### Live-build and serve the app

In one terminal window, run:

    $ pnpm watch

In another, run:

    $ ./run.sh
